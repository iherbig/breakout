#pragma once

#include "types.h"

struct Color {
    f32 red;
    f32 green;
    f32 blue;
};