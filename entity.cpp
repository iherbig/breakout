#include "types.h"
#include "entity.h"

Entity make_entity(glm::vec2 position, f32 width, f32 height, EntityType type, f32 red, f32 green, f32 blue, f32 alpha) {
    assert(red <= 1.0f);
    assert(green <= 1.0f);
    assert(blue <= 1.0f);
    assert(alpha <= 1.0f);

    Entity result;
    result.color = { red, green, blue };
    result.bounding_box.x = width;
    result.bounding_box.y = height;
    result.position = position;
    result.type = type;

    return result;
}

Entity make_entity(glm::vec2 position, f32 width, f32 height, EntityType type) {
    return make_entity(position, width, height, type, 1.0f, 1.0f, 1.0f, 1.0f);
}