#pragma once

#include "math.h"
#include "color.h"

enum EntityType {
    Entity_None,
    Entity_Wall,
    Entity_Brick,
    Entity_Paddle,
    Entity_Ball
};

struct Entity {
    EntityType type;

    glm::vec2 velocity;
    glm::vec2 position;
    glm::vec2 bounding_box;

    Color color;
};

Entity make_entity(glm::vec2 position, f32 width, f32 height, EntityType type);
Entity make_entity(glm::vec2 position, f32 width, f32 height, EntityType type, f32 red, f32 green, f32 blue, f32 alpha);