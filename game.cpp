#include <stdio.h>

#include "types.h"
#include "render.h"
#include "game.h"

void render_game(Game *game) {
    use_shader(Shader_Default);

    auto paddle = game->world->paddle;
    draw_quad(paddle->position, paddle->bounding_box, paddle->color.red, paddle->color.green, paddle->color.blue, 1.0f);

    auto ball = game->world->ball;
    draw_quad(ball->position, ball->bounding_box, ball->color.red, ball->color.green, ball->color.blue, 1.0f);

    for (auto index = 0; index < array_len(game->world->walls); ++index) {
        auto wall = game->world->walls[index];
        draw_quad(wall->position, wall->bounding_box, wall->color.red, wall->color.green, wall->color.blue, 1.0f);
    }

    for (auto index = 0; index < array_len(game->world->bricks); ++index) {
        auto brick = game->world->bricks[index];
        draw_quad(brick->position, brick->bounding_box, brick->color.red, brick->color.green, brick->color.blue, 1.0f);
    }
}
