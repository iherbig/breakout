#pragma once

#include "opengl.h"
#include "world.h"
#include "types.h"
#include "input.h"

struct Game {
    World *world;
    GLFWwindow *window;
};

void render_game(Game *game);
