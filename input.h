#pragma once

#include "math.h"

struct Input {
    bool left;
    bool right;
    bool up;
    bool down;
    bool space;

    glm::vec2 mouse_position;
};