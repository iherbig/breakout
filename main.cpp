#include <assert.h>

#include <stdio.h>

#include "opengl.h"
#include "game.h"
#include "entity.h"
#include "math.h"
#include "types.h"
#include "render.h"
#include "game.h"

Input process_input(GLFWwindow *window) {
    Input result;
    
    { 
        auto state = glfwGetKey(window, GLFW_KEY_LEFT);
        result.left = state == GLFW_PRESS || state == GLFW_REPEAT;
    }
    { 
        auto state = glfwGetKey(window, GLFW_KEY_RIGHT);
        result.right = state == GLFW_PRESS || state == GLFW_REPEAT;
    }
    { 
        auto state = glfwGetKey(window, GLFW_KEY_UP);
        result.up = state == GLFW_PRESS || state == GLFW_REPEAT;
    }
    { 
        auto state = glfwGetKey(window, GLFW_KEY_DOWN);
        result.down = state == GLFW_PRESS || state == GLFW_REPEAT;
    }
    { 
        auto state = glfwGetKey(window, GLFW_KEY_SPACE);
        result.space = state == GLFW_PRESS || state == GLFW_REPEAT;
    }
    {
        double x_pos, y_pos;
        glfwGetCursorPos(window, &x_pos, &y_pos);
        result.mouse_position.x = (f32)x_pos;
    }
    
    return result;
}

void error_callback(int error, const char *description) {
    fprintf(stderr, "Error: %s\n", description);
}

int main() {
    auto windowWidth = 800.0f;
    auto windowHeight = 600.0f;
    
    //
    // Initialize glfw
    //
    
    GLFWwindow *window;
    
    if (!glfwInit()) {
        return -1;
    }
    
    glfwSetErrorCallback(error_callback);
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    
    window = glfwCreateWindow((int)windowWidth, (int)windowHeight, "Breakout", NULL, NULL);
    
    if (!window) {
        glfwTerminate();
        return -1;
    }
    
    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
    
    // 
    // Initialize rendering stuff
    //
    
    glViewport(0, 0, (int)windowWidth, (int)windowHeight);
    
    init_camera();
    init_projection((f32)windowWidth, (f32)windowHeight);
    
    load_default_shaders();
    
    // 
    // Initialize game
    //
    
    World world = {};
    Game game = {};
    game.world = &world;
    game.window = window;
    
    //
    // Add entities
    //
    
    // Ensure first entity is null entity
    add_entity(&world, make_entity(glm::vec2(0.0f, 0.0f), 0, 0, Entity_None));
    
    auto wall_width = 10.0f;
    
    // top
    add_wall(&world,
             glm::vec2(windowWidth / 2, windowHeight - (wall_width / 2)),
             windowWidth, wall_width);
    
    // bottom
    add_wall(&world,
             glm::vec2(windowWidth / 2, wall_width / 2),
             windowWidth, wall_width);
    
    // left
    add_wall(&world,
             glm::vec2((wall_width / 2), windowHeight / 2),
             wall_width, windowHeight);
    
    // right
    add_wall(&world,
             glm::vec2(windowWidth - (wall_width / 2), windowHeight / 2),
             wall_width, windowHeight);
    
    auto brick_width = 33.0f;
    auto brick_height = 11.0f;
    auto initial_brick_x = windowWidth / 5 + (brick_width / 2);
    auto initial_brick_y = windowHeight / 2;
    auto brick_position = glm::vec2(initial_brick_x, initial_brick_y);
    auto num_rows_of_bricks = 20;
    auto num_columns_of_bricks = 15;
    auto red = 0.0f;
    auto green = 0.0f;
    auto blue = 0.0f;
    auto wavelength = 400.0f;
    
    for (auto col = 0; col < num_columns_of_bricks; ++col) {
        for (auto row = 0; row < num_rows_of_bricks; ++row) {
            spectral_color(&red, &green, &blue, wavelength++);
            add_brick(&world, brick_position, brick_width, brick_height, red, green, blue);
            brick_position.y += brick_height;
        }
        
        brick_position.x += brick_width;
        brick_position.y = initial_brick_y;
    }
    
    auto paddleHeight = 15.0f;
    auto paddleWidth = 90.0f;
    
    add_paddle(&world, glm::vec2(windowWidth / 2, (windowHeight / 5)), paddleWidth, paddleHeight);
    
    auto ball_side = 10.0f;
    
    auto ball_position = world.paddle->position;
    ball_position.y += world.paddle->bounding_box.y / 2  + ball_side / 2 + 1;
    add_ball(&world, ball_position, ball_side, ball_side);
    
    auto thisFrameTime = glfwGetTime();
    auto lastFrameTime = thisFrameTime;
    
    auto frame_number = 0;
    
    while (!glfwWindowShouldClose(window)) {
        frame_number++;
        
        auto input = process_input(game.window);
        
        thisFrameTime = glfwGetTime();
        auto dt = thisFrameTime - lastFrameTime;
        
        printf("FPS %f\n", 1 / dt);
        
        glClearColor(35.0f / 255.0f, 30.0f / 255.0f, 127.0f / 255.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        
        update_world(&world, &input, frame_number, (f32)dt);
        
        render_game(&game);
        
        glfwSwapBuffers(window);
        
        lastFrameTime = thisFrameTime;
        
        glfwPollEvents();
    }
    
    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}
