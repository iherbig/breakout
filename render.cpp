#include <stdio.h>

#include "opengl.h"
#include "render.h"
#include "types.h"

const char *default_vertex_shader_source = 
"#version 330 core\n \
in vec3 aPos;\n \
uniform mat4 transform;\n \
void main()\n \
{\n \
    gl_Position = transform * vec4(aPos, 1.0f);\n \
}";

const char *default_fragment_shader_source = 
"#version 330 core\n \
out vec4 FragColor;\n \
uniform vec4 ourColor; \
void main()\n \
{\n \
   FragColor = ourColor;\n \
}";

static GLuint active_shader_program = -1;
static GLuint default_shader_program = -1;;

static GLuint quad_vao = -1;
static glm::mat4 view;
static glm::mat4 proj;

void use_shader(ShaderType type) {
    // switch (type) {
    //     default:
    //     {
            glUseProgram(default_shader_program);
            active_shader_program = default_shader_program;
    //     }
    // }
}

static void use_color(f32 red, f32 green, f32 blue, f32 alpha) {
    auto uniform = glGetUniformLocation(active_shader_program, "ourColor");
    glUniform4f(uniform, red, green, blue, alpha);
}

static u32 make_vao(glm::vec2 top_right, glm::vec2 bottom_right, glm::vec2 bottom_left, glm::vec2 top_left) {
    f32 vertices[] = {    top_right.x,    top_right.y, 0.0f, 
                       bottom_right.x, bottom_right.y, 0.0f,
                        bottom_left.x,  bottom_left.y, 0.0f,
                           top_left.x,     top_left.y, 0.0f };
    u32 indices[] = { 0, 1, 3,
                     1, 2, 3 };

    u32 vbo, vao, ebo;
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);

    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(f32), 0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    return vao;
}

void load_default_shaders() {
    auto vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, &default_vertex_shader_source, NULL);
    glCompileShader(vertShader);

    int success;
    glGetShaderiv(vertShader, GL_COMPILE_STATUS, &success);
    if (!success) {
        char errorBuffer[512];
        glGetShaderInfoLog(vertShader, 512, NULL, errorBuffer);
        fprintf(stderr, "DEFAULT VERT ERROR: %s\n", errorBuffer);
    }

    auto fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, &default_fragment_shader_source, NULL);
    glCompileShader(fragShader);

    glGetShaderiv(fragShader, GL_COMPILE_STATUS, &success);
    if (!success) {
        char errorBuffer[512];
        glGetShaderInfoLog(fragShader, 512, NULL, errorBuffer);
        fprintf(stderr, "DEFAULT FRAG ERROR: %s\n", errorBuffer);
    }

    default_shader_program = glCreateProgram();
    glAttachShader(default_shader_program, vertShader);
    glAttachShader(default_shader_program, fragShader);
    glLinkProgram(default_shader_program);

    glGetProgramiv(default_shader_program, GL_LINK_STATUS, &success);
    if (!success) {
        char errorBuffer[512];
        glGetProgramInfoLog(default_shader_program, 512, NULL, errorBuffer);
        fprintf(stderr, "DEFAULT SHADER LINK ERROR: %s\n", errorBuffer);
        default_shader_program = -1;
    }

    glDeleteShader(vertShader);
    glDeleteShader(fragShader);
}

void use_transform(glm::mat4 transform) {
    auto uniform = glGetUniformLocation(active_shader_program, "transform");
    glUniformMatrix4fv(uniform, 1, GL_FALSE, glm::value_ptr(transform));
}

void init_camera() {
    view = glm::lookAt(glm::vec3(0.0f, 0.0f, 3.0f),
                       glm::vec3(0.0f, 0.0f, 0.0f),
                       glm::vec3(0.0f, 1.0f, 0.0f));
}

void init_projection(f32 width, f32 height) {
    proj = glm::ortho(0.0f, width, 0.0f, height, 0.1f, 100.0f);
}

void draw_quad(glm::vec2 position, glm::vec2 size, f32 red, f32 green, f32 blue, f32 alpha)
{
    if (quad_vao == -1) {
        quad_vao = make_vao(glm::vec2( 1.0f,  1.0f),
                            glm::vec2( 1.0f, -1.0f),
                            glm::vec2(-1.0f, -1.0f),
                            glm::vec2(-1.0f,  1.0f));
    }

    // glm seems to handle this in the reverse order from what I expected.
    // Rather than calling the glm functions in the order that they need to
    // be applied to the underlying transformation matrix, you call them
    // in the order that you would read them from left-to-right.
    // T * R * S * vec -> translate(model); rotate(model); scale(model); model * vec;
    glm::mat4 model;
    model = glm::translate(model, glm::vec3(position, 1.0f));
    model = glm::scale(model, glm::vec3(size.x / 2, size.y / 2, 1.0f));

    auto transform = proj * view * model;

    use_color(red, green, blue, alpha);
    use_transform(transform);

    glBindVertexArray(quad_vao);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
}

void draw_circle(glm::vec2 position, f32 radius, f32 red, f32 green, f32 blue, f32 alpha) {
}

// This implementation taken from: https://stackoverflow.com/a/22681410
void spectral_color(f32 *r, f32 *g, f32 *b, f32 wavelength) {
    f32 t;
    *r = 0.0f;
    *g = 0.0f;
    *b = 0.0f;
    
    if ((wavelength >= 400.0f) && (wavelength < 410.0f)) {
        t = (wavelength - 400.0f) / (410.0f - 400.0f);
        *r = +(0.33f * t) - (0.20f * t * t);
    }
    else if ((wavelength >= 410.0f) && (wavelength < 475.0f)) {
        t = (wavelength - 410.0f) / (475.0f - 410.0f);
        *r = 0.14f - (0.13f * t * t);
    }
    else if ((wavelength >= 545.0f) && (wavelength < 595.0f)) {
        t = (wavelength - 545.0f) / (595.0f - 545.0f);
        *r = +(1.98f * t) - (t * t);
    }
    else if ((wavelength >= 595.0f) && (wavelength < 650.0f)) {
        t = (wavelength - 595.0f) / (650.0f - 595.0f);
        *r = 0.98f + (0.06f * t) - (0.40f * t * t);
    }
    else if ((wavelength >= 650.0f) && (wavelength < 700.0f)) {
        t = (wavelength - 650.0f) / (700.0f - 650.0f);
        *r = 0.65f - (0.84f * t) + (0.20f * t * t);
    }

    if ((wavelength >= 415.0f) && (wavelength < 475.0f)) {
        t = (wavelength - 415.0f) / (475.0f - 415.0f);
        *g = (0.80f * t * t);
    }
    else if ((wavelength >= 475.0f) && (wavelength < 590.0f)) {
        t = (wavelength - 475.0f) / (590.0f - 475.0f);
        *g = 0.8f + (0.76f * t) - (0.80f * t * t);
    }
    else if ((wavelength >= 585.0f) && (wavelength < 639.0f)) {
        t = (wavelength - 585.0f) / (639.0f - 585.0f);
        *g = 0.84f - (0.84f * t);
    }

    if ((wavelength >= 400.0f) && (wavelength < 475.0f)) {
        t = (wavelength - 400.0f) / (475.0f - 400.0f);
        *b = +(2.20f * t) - (1.50f * t * t);
    }
    else if ((wavelength >= 475.0f) && (wavelength < 560.0f)) {
        t = (wavelength - 475.0f) / (560.0f - 475.0f);
        *b = 0.7f - (t) + (0.30f * t * t);
    }
}