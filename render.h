#pragma once

#include "types.h"
#include "math.h"

enum ShaderType {
    Shader_Default,
};

void use_shader(ShaderType type);
void load_default_shaders();
void draw_quad(glm::vec2 position, glm::vec2 size, f32 red, f32 green, f32 blue, f32 alpha);
void draw_circle(glm::vec2 position, f32 radius, f32 red, f32 green, f32 blue, f32 alpha);
void init_camera();
void init_projection(f32 width, f32 height);
u32 make_vao(glm::vec2 top_right, glm::vec2 bottom_right, glm::vec2 bottom_left, glm::vec2 top_left);
void spectral_color(f32 *r, f32 *g, f32 *b, f32 wavelength);