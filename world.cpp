#include <assert.h>
#include <stdio.h>

#include "math.h"
#include "world.h"
#include "render.h"
#include "input.h"

void update_world(World *world, Input *input, u32 frame_number, f32 dt) {
    //
    // Update ball
    //

    if (input->space && world->ball->velocity.x == 0) {
        world->ball->velocity = glm::vec2(frame_number % 2 ? 250.0f : -250.0f, 175.0f);
    }

    // NOTE(iherbig): The ball never accelerates, so the rate it's moving is pretty
    // static. Just check endpoint if it's inside the bounds of any other entity to
    // determine collision.
    auto ball = world->ball;
    auto potential_ball_position = ball->position + (ball->velocity * dt);
    for (auto entity_index = 0; entity_index < world->num_entities; ++entity_index) {
        auto entity = &world->entities[entity_index];

        // Check corners of ball bounding box to see which (if any) corner collides with this entity.
        // This will tell us which direction to reflect the ball towards. It'll also tell us
        // how to calculate the final position for this frame.
    }

    ball->position = potential_ball_position;
    
    //
    // Update paddle
    //

    auto paddle = world->paddle;
    paddle->position.x = input->mouse_position.x;
    printf("Paddle at %f\n", paddle->position.x);
}

Entity *add_entity(World *world, Entity entity) {
    auto entity_index = world->num_entities++;
    world->entities[entity_index] = entity;
    assert(max_entities > world->num_entities);

    return &world->entities[entity_index];
}

void add_wall(World *world, glm::vec2 position, f32 width, f32 height) {
    static int num_walls = 0;
    
    auto entity = make_entity(position, width, height, Entity_Wall);

    auto added_entity = add_entity(world, entity);
    world->walls[num_walls++] = added_entity;
    assert(num_walls <= array_len(world->walls));
}

void add_brick(World *world, glm::vec2 position, f32 width, f32 height, f32 red, f32 green, f32 blue) {
    static int num_bricks = 0;

    auto entity = make_entity(position, width, height, Entity_Brick, red, green, blue, 1.0f);

    auto added_entity = add_entity(world, entity);
    world->bricks[num_bricks++] = added_entity;
    assert(num_bricks <= array_len(world->bricks));
}

void add_paddle(World *world, glm::vec2 position, f32 width, f32 height) {
    auto entity = make_entity(position, width, height, Entity_Paddle);
    auto added_paddle = add_entity(world, entity);
    world->paddle = added_paddle;
}

void add_ball(World *world, glm::vec2 position, f32 width, f32 height) {
    auto entity = make_entity(position, width, height, Entity_Ball);
    auto added_ball = add_entity(world, entity);
    world->ball = added_ball;
}