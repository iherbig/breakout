#pragma once

#include <GLFW\glfw3.h>

#include "types.h"
#include "input.h"
#include "entity.h"

const int max_entities = 4096;

struct World {
    int num_entities;
    Entity entities[max_entities];

    Entity *paddle;
    Entity *ball;
    Entity *walls[4];
    Entity *bricks[300];
};

Entity *add_entity(World *world, Entity entity);
void add_wall(World *world, glm::vec2 position, f32 width, f32 height);
void add_brick(World *world, glm::vec2 position, f32 width, f32 height, f32 red, f32 green, f32 blue);
void add_paddle(World *world, glm::vec2 position, f32 width, f32 height);
void add_ball(World *world, glm::vec2 position, f32 width, f32 height);

void update_world(World *world, Input *input, u32 frame_number, f32 dt);